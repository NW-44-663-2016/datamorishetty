﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataMorishetty.Models
{
    public class EventT
    {
        [ScaffoldColumn(false)]
        [Key]
        public int EventID { get; set; }

        [Display(Name = "Event Name")]
        public string EventName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Contact Information")]
        public string PhoneNumber { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Id")]
        public string EmailId { get; set; }

        [Display(Name = "Event Location")]
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }
    }
}
