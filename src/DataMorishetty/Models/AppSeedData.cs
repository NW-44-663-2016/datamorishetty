﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using DataMorishetty.Models;

namespace DataKowshik.Models
{
    public static class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            if (context.Locations.Any())
            {
                return;   // DB already seeded
            }

            var location1 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            var location2 = new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };
            var location3 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            var location4 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
            var location5 = new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
            var location6 = new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };
            var location7 = new Location() { Latitude = 29.9, Longitude = 100.5, Place = "Delhi", Country = "India" };

            context.AddRange(location1, location2, location3, location4, location5, location6, location7);
            context.SaveChanges();

            context.Events.AddRange(
                    new EventT { EventName = "Retro Bash", PhoneNumber = "6605285825", EmailId = "morishettyk@gmail.com", LocationId = location1.LocationID },
                    new EventT { EventName = "Antique Quest and Wine Fest", PhoneNumber = "8133842927", EmailId = "rajesh@gmail.com", LocationId = location2.LocationID },
                    new EventT { EventName = "Barn Blast", PhoneNumber = "3304955827", EmailId = "abhishek@gmail.com", LocationId = location3.LocationID },
                    new EventT { EventName = "Hats In The Garden", PhoneNumber = "3193338853", EmailId = "anup@gmail.com", LocationId = location4.LocationID },
                    new EventT { EventName = "Denim, Dinner & Dance !", PhoneNumber = "8019567249", EmailId = "rakesh@gmail.com", LocationId = location5.LocationID },
                    new EventT { EventName = "Gathering in the Gardens", PhoneNumber = "9892462439", EmailId = "anup@gmail.com", LocationId = location6.LocationID });
            context.SaveChanges();

        }
    }
}

