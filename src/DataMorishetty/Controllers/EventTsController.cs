using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataMorishetty.Models;

namespace DataMorishetty.Controllers
{
    public class EventTsController : Controller
    {
        private AppDbContext _context;

        public EventTsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: EventTs
        public IActionResult Index()
        {
            var appDbContext = _context.Events.Include(e => e.Location);
            return View(appDbContext.ToList());
        }

        // GET: EventTs/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            EventT eventT = _context.Events.Single(m => m.EventID == id);
            if (eventT == null)
            {
                return HttpNotFound();
            }

            return View(eventT);
        }

        // GET: EventTs/Create
        public IActionResult Create()
        {
            ViewData["LocationId"] = new SelectList(_context.Locations, "LocationID", "Location");
            return View();
        }

        // POST: EventTs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(EventT eventT)
        {
            if (ModelState.IsValid)
            {
                _context.Events.Add(eventT);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationId"] = new SelectList(_context.Locations, "LocationID", "Location", eventT.LocationId);
            return View(eventT);
        }

        // GET: EventTs/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            EventT eventT = _context.Events.Single(m => m.EventID == id);
            if (eventT == null)
            {
                return HttpNotFound();
            }
            ViewData["LocationId"] = new SelectList(_context.Locations, "LocationID", "Location", eventT.LocationId);
            return View(eventT);
        }

        // POST: EventTs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EventT eventT)
        {
            if (ModelState.IsValid)
            {
                _context.Update(eventT);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationId"] = new SelectList(_context.Locations, "LocationID", "Location", eventT.LocationId);
            return View(eventT);
        }

        // GET: EventTs/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            EventT eventT = _context.Events.Single(m => m.EventID == id);
            if (eventT == null)
            {
                return HttpNotFound();
            }

            return View(eventT);
        }

        // POST: EventTs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            EventT eventT = _context.Events.Single(m => m.EventID == id);
            _context.Events.Remove(eventT);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
